import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HeroesService } from 'src/app/services/heroes.service';


@Component({
  selector: 'app-result',
  templateUrl: './result.component.html'
})
export class ResultComponent implements OnInit {

  heroes: any[] = [];
  termino: string;

  constructor(private activatedRoute: ActivatedRoute,
              private _heroesService: HeroesService,
              private router: Router) { }
  
  ngOnInit(): void {
    this.activatedRoute.params.subscribe( param => {
      this.termino = param['nombre'];
      this.heroes = this._heroesService.buscarHeroe( param['nombre'] );

      console.log(this.heroes);
    });

  }

  verHeroe( idx: number ) {
    this.router.navigate( ['/heroe', idx] );
  }
}
